try:
    x = 1
except NameError:
    print('Variable is not defined ;(')
else:
    print('Variable defined')

try:
    y
except NameError:
    print('Variable is not defined ;(')
else:
    print('Variable defined')
