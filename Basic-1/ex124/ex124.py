def valori_egale(*vars):
    for i in vars:
        if i != vars[0]:
            return 'Variabel diferite'
    return 'Variabile egale'

print(valori_egale(2, 3, 3, 2))
print(valori_egale(2, 2, 2, 2))
print(valori_egale(1, 2, 3, 4))
