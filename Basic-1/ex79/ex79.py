import sys

string1 = "one"
string2 = "two"
string3 = "three"

x = 0
y = 123
z = 123.123

print("Size of ", string1, ": ", str(sys.getsizeof(string1))+ "bytes")
print("Size of ", string2, ": ", str(sys.getsizeof(string2))+ "bytes")
print("Size of ", string3, ": ", str(sys.getsizeof(string3))+ "bytes")

print("Size of ", x, ": ", str(sys.getsizeof(x))+ "bytes")
print("Size of ", y, ": ", str(sys.getsizeof(y))+ "bytes")
print("Size of ", z, ": ", str(sys.getsizeof(z))+ "bytes")
