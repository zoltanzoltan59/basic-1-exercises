def test(string):
    while '01' in string:
        string = string.replace('01', '')
    return len(string) == 0

string = '01010101'
print('Original string: ', string)
print('Check if every consecutive sequence of zeroes is followed by a consecutive number of ones: ')
print(test(string))

string = '00110011'
print('Original string: ', string)
print('Check if every consecutive sequence of zeroes is followed by a consecutive number of ones: ')
print(test(string))

