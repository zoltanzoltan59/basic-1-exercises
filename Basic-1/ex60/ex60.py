def hipotenusa(x, y):
    h = (x**2 + y**2)**0.5
    return h

print(hipotenusa(3, 4))
print(hipotenusa(3.5, 4.4))
