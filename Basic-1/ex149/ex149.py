def putere_3(n):
    rez = 0
    if n > 0:
        for i in range(n):
            rez += i * i * i
        return rez
    elif n <= 0:
        raise ValueError('Must be positive numbers!')

print('Suma primelor n^3 numere: ', putere_3(3))
print('Suma primelor n^3 numere: ', putere_3(6))
