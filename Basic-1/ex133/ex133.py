from timeit import default_timer

def timer(n):
    start = default_timer()

    for i in range(0, n):
        print(i)
    print(default_timer() - start)

timer(5)
timer(10)
