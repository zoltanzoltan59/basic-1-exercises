def cmmdc(a, b):
    div = 1
    if a % b == 0:
        return b
    for i in range(int(b / 2), 0, -1):
        if a % i == 0 and b % i == 0:
            div = i
            break
    return div

print("Cmmdc: ", cmmdc(12, 24))
print("Cmmdc: ", cmmdc(4, 6))

