from math import pi

r = 6.0
V = 4.0/3.0*pi* r**3

print("Volume of the sphere: ", V)
