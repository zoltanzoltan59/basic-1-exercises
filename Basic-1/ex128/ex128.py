def cifre_mici(string):
    cnt = 0
    for char in string:
        if(ord(char) >= 97 and ord(char) <= 122):
            cnt = cnt + 1
        if cnt > 0:
            return True

print('Verificand textul pentru litere mici')

string = 'AASJD123241csadASD'
print('Original string: ')
print(string)
print('Cifre mici existente:', cifre_mici(string))

string = 'asdASDJSAJajzxn'
print('Original string: ')
print(string)
print('Cifre mici existente: ', cifre_mici(string))
