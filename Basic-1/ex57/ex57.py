import time

def timp_executie(n):
    s_time = time.time()
    s = 0
    for i in range(1, n+1):
        s = s + i
    e_time = time.time()
    return e_time - s_time
n = 5

print(timp_executie(n))
