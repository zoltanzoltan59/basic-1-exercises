import os.path


for file in [__file__, os.path.dirname(__file__), '/', './boken_link']:
    print("File: ", file)
    print("Absolute: ", os.path.isabs(file))
    print("File?: ", os.path.isfile(file))
    print("Dir?: ", os.path.isdir(file))
    print("Link?: ", os.path.islink(file))
    print("Exists?: ", os.path.exists(file))
    print("Link Exists?: ", os.path.lexists(file))
