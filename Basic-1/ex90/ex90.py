def source_cpy(src, dest):
    with open(src) as f, open(dest, 'w') as d:
        d.write(f.read())
source_cpy('ex90.py', 'test.py')
with open('test.py', 'r') as filehandle:
    for l in filehandle:
        print(l, end='')
