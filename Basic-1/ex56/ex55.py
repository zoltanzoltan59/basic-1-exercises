import termios
import struct
import fcntl

def terminal_size():
    th, tw, hp, wp, = struct.unpack('HHHH', fcntl.ioctl(0, termios.TIOCGWINSZ, struct.pack('HHHH', 0, 0, 0, 0)))
    return th, tw

print("Nr of Col and ROws: ", terminal_size())
