Write a Python program to check if a number is positive, negative or zero.

Positive Numbers: Any number above zero is known as a positive number. Positive numbers are written without any sign or a '+' sign in front of them and they are counted up from zero, i.e. 1, + 2, 3, +4 etc.
Negative Numbers: Any number below zero is known as a negative number. Negative numbers are always written with a '−' sign in front of them and they are counted down from zero, i.e. -1, -2, -3, -4 etc.
Always look at the sign in front of a number to check if it is positive or negative. Zero, 0, is neither positive nor negative.
