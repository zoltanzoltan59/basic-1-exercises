Write a Python program to get the ASCII value of a character.

ASCII (Listeni/ˈæski/ ass-kee), abbreviated from American Standard Code for Information Interchange, is a character encoding standard. ASCII codes represent text in computers, telecommunications equipment, and other devices. Most modern character-encoding schemes are based on ASCII, although they support many additional characters.
