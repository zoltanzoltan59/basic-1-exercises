def decimal_hex(numere):
    digit = '0123456789ABCDEF'
    x = (numere % 16)
    rest_part = numere // 16
    if(rest_part == 0):
        return digit[x]
    return decimal_hex(rest_part) + digit[x]

numere = [0, 15, 30, 55, 355, 656, 896, 1125]
print('Decimal numbers: ')
print(numere)

print('Hex numbers of decimal: ')
print([decimal_hex(x) for x in numere])
