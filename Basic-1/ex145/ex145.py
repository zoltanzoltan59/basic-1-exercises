def check_type(string):
    if isinstance(x, tuple) == True:
        return 'Tuple'
    elif isinstance(x, list) == True:
        return 'List'
    elif isinstance(x, set) == True:
        return 'Set'
    else:
        return 'Not one from above'

x = [1, 2, 3, 4, 5]
print(check_type(x))

x = {1, 2, 3, 4, 5}
print(check_type(x))

x = ('tuple', False, 3.2, 1)
print(check_type(x))

x = 12031
print(check_type(x))
