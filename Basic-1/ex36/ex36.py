def verif_int(a, b):
    if not (isinstance(a, int) and isinstance(b, int)):
        return "Input trebuie sa fie de tip int!"
    return a + b
print(verif_int(24, 4))
print(verif_int(24, 123.1231))
print(verif_int("asd)", 12))
