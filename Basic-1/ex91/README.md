Write a Python program to swap two variables.

Python: swapping two variables

Swapping two variables refers to mutually exchanging the values of the variables. Generally, this is done with the data in memory.

The simplest method to swap two variables is to use a third temporary variable :

define swap(a, b)
    temp := a
    a := b
    b := temp
