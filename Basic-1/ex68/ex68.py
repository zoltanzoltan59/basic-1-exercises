def sum_digit():
    n = int(input("Numar: "))
    s = 0
    while n:
        s += n % 10
        n //=10
    return s

print("Suma cifrelor: ", sum_digit())
