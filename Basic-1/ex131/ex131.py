list_var = ['a', 'b', 'c']
x, y, z = (list_var + [None] * 3)[:3]

print(x, y, z)

list_var = [100, 20.25]

x, y = (list_var + [None] * 2)[:2]
print(x, y)
