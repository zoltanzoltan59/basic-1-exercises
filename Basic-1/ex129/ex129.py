def add_zero():
    string = str(input('String: '))
    return string.ljust(8, '0')

print(add_zero())
