import os
import sys
import platform

print('Os-Name                  ', os.name)
print('Operation System         ', sys.platform)
print('Platform System          ', platform.system())
print('Platform machine         ', platform.machine())
print('Platform architecture    ', platform.architecture())
