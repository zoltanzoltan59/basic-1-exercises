def list_sum(n):
    return sum(n)
n = [10, 20, 30]
print("Original List: ", n)
print(type(n))
print("Sum: ", list_sum(n))

def dict_sum(n1):
   s1 = 0
   for i in n1:
       s1 = s1 + n1[i]     
   return s1
n1 = {'a': 100, 'b':200, 'c':300, 'd':120}
print("Original Dictionary:", n1)
print(type(n1))
print("Sum: ", dict_sum(n1))

def set_sum(n2):
    s = sum(n2)
    return str(s)

n2 = {7, 4, 9, 1, 3, 2}
print("Original set: ", n2)
print(type(n2))
print("Sum: ", set_sum(n2))

def tuple_sum(n3):
    s = sum(n3)
    return str(s)
n3 = (1, 2, 3, 4)
print("Original tuple: ", n3)
print(type(n3))
print("Sum: ", tuple_sum(n3))
