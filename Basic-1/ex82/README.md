Write a Python program to calculate the sum of all items of a container (tuple, list, set, dictionary).

Some objects contain references to other objects; these are called containers. Generally, containers provide a way to access the contained objects and to iterate over them. Examples of containers are lists, sets, tuples and dictionaries.
