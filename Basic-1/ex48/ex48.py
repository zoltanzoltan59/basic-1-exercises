def test(n):
    try:
        return int(n)
    except ValueError:
        return float(n)

print(test(123))
print(test(123.123))

