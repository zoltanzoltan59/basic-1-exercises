kilopas = float(input("KiloPascal: "))
psi = kilopas / 6.89475
mmhg = kilopas * 760 / 101.325
atm = kilopas / 101.325

print("Pressure pounds per square inch: %.2f psi" %(psi))
print("Pressure millimeter of mercury: %.2f mmhg" %(mmhg))
print("Atmosphere pressure: %.2f atm" %(atm))
