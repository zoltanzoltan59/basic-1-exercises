def odd_prod(data):
    for i in range(len(data)):
        for j in range(len(data)):
            if i != j:
                prod = data[i] * data[j]
                if prod & 1:
                    return True
    return False

data = [1, 2, 3, 4]
print(odd_prod(data))

data = [2, 3, 4]
print(odd_prod(data))
