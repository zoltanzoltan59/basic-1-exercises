dict = {'k1':' v1', 'k2': 'v2', 'k3': 'v3', 'k4': 'v4'}
print('Print specific key/value')

x, y = list(dict.items())[0]
print(x, y)

x, y = list(dict.items())[2]
print(x, y)
