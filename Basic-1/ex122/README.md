Write a Python program to empty a variable without destroying it.

type() returns the type of an object, which when called produces an 'empty' new value.

Sample data: n=20
d = {"x":200}
Expected Output: 0
{}
