def convert():
    ft = int(input("Feet: "))
    inches = ft * 12
    yards = ft / 3.0
    miles = ft / 5280.0
    return inches, yards, miles

print(convert())
